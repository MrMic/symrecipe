-- Doctrine Migration File Generated on 2023-09-11 10:50:51

-- Version DoctrineMigrations\Version20230911104341
ALTER TABLE ingredient ADD user_id INT NOT NULL;
ALTER TABLE ingredient ADD CONSTRAINT FK_6BAF7870A76ED395 FOREIGN KEY (user_id) REFERENCES user (id);
CREATE INDEX IDX_6BAF7870A76ED395 ON ingredient (user_id);
-- Version DoctrineMigrations\Version20230911104341 update table metadata;
INSERT INTO doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\\Version20230911104341', '2023-09-11 10:50:51', 0);
