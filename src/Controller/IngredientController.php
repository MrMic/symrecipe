<?php

namespace App\Controller;

use App\Entity\Ingredient;
use App\Form\IngredientType;
use App\Repository\IngredientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
// use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class IngredientController extends AbstractController
{
    /**
     * This controller displays all the ingredients
     *
     * @param PaginatorInterface   $paginator
     * @param IngredientRepository $repository
     * @param Request              $request
     *
     * @return Response
     * */
    #[Route('/ingredient', name: 'ingredient.index')]
    #[IsGranted('ROLE_USER')]
    public function index(
        IngredientRepository $repository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        // dd($repository->findAll());
        $ingredients = $paginator->paginate(
            $repository->findBy(['user' => $this->getUser()]),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render(
            'pages/ingredient/index.html.twig',
            [
                'ingredients' => $ingredients
            ]
        );
    }

    /**
     * This controller displays a form to create a new ingredient
     *
     * @param Request                $request
     * @param EntityManagerInterface $manager
     *
     * @return Response
     * */
    #[Route('/ingredient/nouveau', name: 'ingredient.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function new(Request $request, EntityManagerInterface $manager): Response
    {
        $ingredient = new Ingredient();
        $form = $this->createForm(IngredientType::class, $ingredient);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $ingredient = $form->getData();
            $ingredient->setUser($this->getUser());

            $manager->persist($ingredient);
            $manager->flush();

            $this->addFlash(
                'success',
                'L\'ingrédient a bien été ajouté'
            );

            return $this->redirectToRoute('ingredient.index');
        }

        return $this->render(
            'pages/ingredient/new.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * This controller displays a form to edit an ingredient
     *
     * @param Ingredient             $ingredient
     * @param Request                $request
     * @param EntityManagerInterface $manager
     * @param Int                    $id
     *
     * @return Response
     */
    #[Route('/ingredient/edition/{id}', name: 'ingredient.edit', methods: ['GET', 'POST'])]
    // #[Security("is_granted('ROLE_USER') and user === ingredient.getUser()")]
    #[IsGranted(
        new Expression('is_granted("ROLE_USER") and user === subject.getUser()'),
        subject: 'ingredient',
    )]
    public function edit(
        Ingredient $ingredient,
        Request $request,
        EntityManagerInterface $manager,
        Int $id
    ): Response {
        if ($this->getUser() !== $ingredient->getUser()) {
            $this->addFlash(
                'warning',
                'Vous n\'êtes pas l\'utilisateur propriétaire de cet ingrédient'
            );
            return $this->redirectToRoute("ingredient.index");
        }

        // $ingredient = $repository->findOneBy(['id' => $id]);
        $form = $this->createForm(IngredientType::class, $ingredient);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $ingredient = $form->getData();

            $manager->persist($ingredient);
            $manager->flush();

            $this->addFlash(
                'success',
                'L\'ingrédient a bien été ajouté'
            );

            return $this->redirectToRoute('ingredient.index');
        }

        return $this->render(
            'pages/ingredient/edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * This controller displays a form to delete an ingredient
     *
     * @param EntityManagerInterface $manager
     * @param IngredientRepository   $repository
     * @param Int                    $id
     *
     * @return Response
     * */
    #[Route('/ingredient/suppression/{id}', name: 'ingredient.delete', methods: ['GET'])]
    public function delete(
        EntityManagerInterface $manager,
        IngredientRepository $repository,
        Int $id
    ): Response {

        $ingredient = $repository->findOneBy(['id' => $id]);
        if (!$ingredient) {
            $this->addFlash(
                'warning',
                'L\'ingrédient en question n\'a pas été trouvé !'
            );
            return $this->redirectToRoute('ingredient.index');
        }

        $manager->remove($ingredient);
        $manager->flush();

        $this->addFlash(
            'success',
            'L\'ingrédient a été supprimé avec success'
        );

        return $this->redirectToRoute('ingredient.index');
    }
}
