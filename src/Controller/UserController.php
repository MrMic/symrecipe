<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserPasswordType;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class UserController extends AbstractController
{
    /**
     * Edition d'un utilisateur
     *
     * @param  User                        $chosenUser
     * @param  Request                     $request
     * @param  EntityManagerInterface      $em
     * @param  UserPasswordHasherInterface $passwordHasher
     * @return Response
     */
    #[Route(
        '/utilisateur/edition/{id}',
        name: 'user.edit',
        methods: [
            'GET',
            'POST',
        ]
    )]
    #[IsGranted(
        new Expression('is_granted("ROLE_USER") and user === subject'),
        subject: 'chosenUser',
    )]
    public function edit(
        User $chosenUser,
        Request $request,
        EntityManagerInterface $em,
        UserPasswordHasherInterface $passwordHasher
    ): Response {

        $form = $this->createForm(UserType::class, $chosenUser);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($passwordHasher->isPasswordValid($user, $form->getData()->getPlainPassword())) {
                $user = $form->getData();
                $em->persist($user);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Les informations de votre compte ont bien été modifié avec succès',
                );

                return $this->redirectToRoute('recipe.index');
            } else {
                $this->addFlash(
                    'warning',
                    'Le mot passe renseigné est incorrect',
                );
            }
        }

        return $this->render(
            'pages/user/edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * Edition du mot de passe
     *
     * @param  User                        $chosenUser
     * @param  Request                     $request
     * @param  UserPasswordHasherInterface $passwordHasher
     * @param  EntityManagerInterface      $em
     * @return Response
     */
    #[IsGranted(
        new Expression('is_granted("ROLE_USER") and user === subject'),
        subject: 'chosenUser',
    )]
    #[Route('/utilisateur/edition-mot-de-passe/{id}', 'user.edit.password', methods: ['GET', 'POST'])]
    public function editPassword(
        User $chosenUser,
        Request $request,
        UserPasswordHasherInterface $passwordHasher,
        EntityManagerInterface $em
    ): Response {
        $form = $this->createForm(UserPasswordType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($passwordHasher->isPasswordValid(
                $chosenUser,
                $form->getData()['plainPassword'],
            )
            ) {
                $chosenUser->setUpdatedAt(new \DateTimeImmutable());
                $chosenUser->setPlainPassword(
                    $form->getData()['newPassword'],
                );
                

                $em->persist($chosenUser);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Le mot de passe a bien été modifié avec succès',
                );


                return $this->redirectToRoute('recipe.index');
            } else {
                $this->addFlash(
                    'warning',
                    'Le mot passe renseigné est incorrect',
                );
            }

        }

        return $this->render(
            'pages/user/edit_password.html.twig', [
            'form' => $form->createView(),
            ]
        );
    }
}
