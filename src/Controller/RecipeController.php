<?php

namespace App\Controller;

use App\Entity\Mark;
use App\Entity\Recipe;
use App\Form\MarkType;
use App\Form\RecipeType;
use App\Repository\MarkRepository;
use App\Repository\RecipeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
// use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

class RecipeController extends AbstractController
{
    /**
     * This controller displays all the recipes
     *
     ** @param PaginatorInterface $paginator
     ** @param RecipeRepository   $repository
     ** @param Request            $request
     *
     * @return Response
     */
    #[Route('/recette', name: 'recipe.index', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function index(
        PaginatorInterface $paginator,
        RecipeRepository $repository,
        Request $request
    ): Response {
        $recipes = $paginator->paginate(
            $repository->findBy(['user' => $this->getUser()]),
            $request->query->getInt('page', 1),
            10
        );


        return $this->render(
            'pages/recipe/index.html.twig',
            [
                'recipes' => $recipes,
            ]
        );
    }


    /**
     * Controller access to all public recipes
     *
     * @return Response
     */
    #[Route('/recette/publique', name: 'recipe.index.public', methods: ['GET'])]
    public  function indexPublic(
        RecipeRepository $repository,
        PaginatorInterface $paginator,
        Request $request,
    ) : Response { 
        $recipes = $paginator->paginate(
            $repository->findPublicRecipe(null),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render(
            'pages/recipe/index_public.html.twig',
            [
                'recipes' => $recipes, 
            ]
        );
    }

    /**
     * Render Recipe 
     *
     * @param  Recipe  $recipe 
     * @param  Request $request
     * @return Response
     */
    #[IsGranted(
        attribute: new Expression('is_granted("ROLE_USER") and  subject.getIsPublic() === true'),
        subject: 'recipe',
    )]
    #[Route('/recette/{id}', name: 'recipe.show', methods: ['GET', 'POST'])]
    public function show(
        Recipe $recipe,
        Request $request,
        MarkRepository $markRepository,
        EntityManagerInterface $manager,
    ): Response {
        $mark = new Mark();
        $form = $this->createForm(MarkType::class, $mark);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $mark->setUser($this->getUser())
                ->setRecipe($recipe);

            $existingMark = $markRepository->findOneBy(
                [
                'user' => $this->getUser(),
                'recipe' => $recipe,
                ]
            );

            if (!$existingMark) {
                $manager->persist($mark);
            } else {
                $existingMark->setMark($form->getData()->getMark());
            }

            $manager->flush();

            $this->addFlash(
                'success',
                'La note a bien été enregistrée'
            );

            return $this->redirectToRoute(
                'recipe.show', [
                'id' => $recipe->getId(),
                ]
            );
        }

        return $this->render(
            'pages/recipe/show.html.twig',
            [
                'recipe' => $recipe,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * This controller displays a form to create a new recipe
     *
     * @param Request                $request
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    #[Route('/recette/creation', name: 'recipe.new', methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function new(Request $request, EntityManagerInterface $em): Response
    {
        $recipe = new Recipe();
        $form = $this->createForm(RecipeType::class, $recipe);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $recipe = $form->getData();
            $recipe->setUser($this->getUser());

            $em->persist($recipe);
            $em->flush();

            $this->addFlash('success', 'La recette a bien été ajoutée');

            return $this->redirectToRoute('recipe.index');
        }

        return $this->render(
            'pages/recipe/new.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }


    /**
     * This controller displays a form to edit an recipe
     *
     * @param Request                $request
     * @param EntityManagerInterface $manager
     * @param RecipeRepository       $repository
     * @param Int                    $id
     *
     * @return Response
     */
    #[Route('/recette/edition/{id}', name: 'recette.edit', methods: ['GET', 'POST'])]
    // #[IsGranted("ROLE_USER")]
    #[IsGranted(
        new Expression('is_granted("ROLE_USER") and user === subject.getUser()'),
        subject: 'recipe',
    )]
    public function edit(
        Recipe $recipe,
        Request $request,
        EntityManagerInterface $manager,
        // RecipeRepository $repository,
        // Int $id
    ): Response {

        // if ($this->getUser() !== $recipe->getUser()) {
        //     $this->addFlash(
        //         'warning',
        //         'Vous n\'êtes pas l\'utilisateur propriétaire de cette recette'
        //     );
        //     return $this->redirectToRoute("recipe.index");
        // }

        // $recipe = $repository->findOneBy(['id' => $id]);
        $form = $this->createForm(RecipeType::class, $recipe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $recipe = $form->getData();

            $manager->persist($recipe);
            $manager->flush();

            $this->addFlash(
                'success',
                'L\'ingrédient a bien été ajouté'
            );

            return $this->redirectToRoute('recipe.index');
        }

        return $this->render(
            'pages/recipe/edit.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }


    /**
     * This controller displays a form to delete an recipe
     *
     * @param EntityManagerInterface $manager
     * @param RecipeRepository       $repository
     * @param Int                    $id
     *
     * @return Response
     */
    #[Route('/recette/suppression/{id}', name: 'recette.delete', methods: ['GET', 'POST'])]
    public function delete(
        EntityManagerInterface $manager,
        RecipeRepository $repository,
        Int $id
    ): Response {
        $recipe = $repository->findOneBy(['id' => $id]);
        if (!$recipe) {
            $this->addFlash(
                'warning',
                'La recette en question n\'a pas été trouvé !'
            );
            return $this->redirectToRoute('recipe.index');
        }

        $manager->remove($recipe);
        $manager->flush();

        $this->addFlash(
            'success',
            'La recette a été supprimé avec success'
        );

        return $this->redirectToRoute('recipe.index');
    }
}
