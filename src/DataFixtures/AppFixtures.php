<?php

namespace App\DataFixtures;

use App\Entity\Ingredient;
use App\Entity\Mark;
use App\Entity\Recipe;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

class AppFixtures extends Fixture
{
    /*
     * @var Generator
     */
    private Generator $faker;


    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager): void
    {

        /*         ╭──────────────────────────────────────────────────────────╮
                   │                          USERS                           │
                   ╰──────────────────────────────────────────────────────────╯
        */

        $users = [];
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setFullName($this->faker->name())
                ->setPseudo(mt_rand(0, 1) == 1 ? $this->faker->firstName() : null)
                ->setEmail($this->faker->email())
                ->setRoles(['ROLE_USER'])
                ->setUpdatedAt(new \DateTimeImmutable('now'))
                ->setPlainPassword('password');

            $users[] = $user;
            $manager->persist($user);
        }



        // $product = new Product();
        // $manager->persist($product);

        /*         ╭──────────────────────────────────────────────────────────╮
                   │                       INGREDIENTS                        │
                   ╰──────────────────────────────────────────────────────────╯
        */

        $ingredients = [];
        for ($i = 0; $i < 50; $i++) {
            $ingredient = new Ingredient();
            $ingredient->setName($this->faker->word())
                ->setPrice(mt_rand(min: 1, max: 100))
                ->setUser($users[mt_rand(0, count($users) - 1)]);

            $ingredients[] = $ingredient;
            $manager->persist($ingredient);
        }


        /*         ╭──────────────────────────────────────────────────────────╮
                   │                         RECIPES                          │
                   ╰──────────────────────────────────────────────────────────╯
        */

        $recipes = [];
        for ($j = 0; $j < 25; $j++) {
            $recipe = new Recipe();
            $recipe->setName($this->faker->word())
                ->setTime(mt_rand(min: 1, max: 1441))
                ->setNbPeople(mt_rand(min: 1, max: 51))
                ->setDifficulty(mt_rand(min: 1, max: 5))
                ->setDescription($this->faker->text(300))
                ->setPrice(mt_rand(min: 1, max: 1000))
                ->setIsFavorite(mt_rand(0, 1) == 1 ? true : false)
                ->setIsPublic(mt_rand(0, 1) == 1 ? true : false)
                ->setUser($users[mt_rand(0, count($users) - 1)]);

            for ($k = 0; $k < mt_rand(min: 1, max: 15); $k++) {
                $recipe->addIngredient($ingredients[mt_rand(0, count($ingredients) - 1)]);
            }

            $recipes[] = $recipe;
            $manager->persist($recipe);
        }

        /* ╭──────────────────────────────────────────────────────────╮
           │                          MARKS                           │
           ╰──────────────────────────────────────────────────────────╯
        */

        foreach ($recipes as $recipe ) {
            for ($i=0; $i < mt_rand(min: 0, max: 4); $i++) { 
                $mark = new Mark();
                $mark->setMark(mt_rand(min: 1, max: 5))
                    ->setUser($users[mt_rand(0, count($users) - 1)])
                    ->setRecipe($recipe);

                $manager->persist($mark);
            }
        }




        $manager->flush();
    }
}
